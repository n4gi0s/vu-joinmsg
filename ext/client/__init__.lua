Events:Subscribe('Extension:Loaded', function()
    WebUI:Init()
end)

NetEvents:Subscribe('PlayerJoin', function(player)
  WebUI:ExecuteJS('join("' .. player .. '");');
end)

NetEvents:Subscribe('PlayerLeave', function(player)
  WebUI:ExecuteJS('leave("' .. player .. '");');
end)