Events:Subscribe('Player:Joining', function(name, playerGuid, ipAddress, accountGuid)
	NetEvents:Broadcast('PlayerJoin', name);
end)

Events:Subscribe('Player:Left', function(player)
	NetEvents:Broadcast('PlayerLeave', player.name);
end)