window.join = (player) => {
	const Toast = Swal.mixin({
		toast: true,
		position: 'top-start',
		showConfirmButton: false,
		timer: 5000
	})

	Toast.fire({
		title: '<i class="fas fa-sign-in-alt"></i>',
		text: player + ' joined the server.',
		background: '#94c5cc'
	})
};

window.leave = (player) => {
	const Toast = Swal.mixin({
		toast: true,
		position: 'top-start',
		showConfirmButton: false,
		timer: 2000
	})

	Toast.fire({
		title: '<i class="fas fa-sign-out-alt"></i>',
		text: player + ' left the server.',
		background: '#94c5cc'
	})
};
